module Main where

import ArithmeticParser.Generator qualified
import ArithmeticParser.Main qualified
import BugParser1.Generator qualified
import BugParser1.Main qualified
import BugParser2.Generator qualified
import BugParser2.Main qualified

import Relude

main :: IO ()
main =
  ArithmeticParser.Generator.main *>
  BugParser1.Generator.main *>
  BugParser2.Generator.main

-- main :: IO ()
-- main = ArithmeticParser.Main.main

-- main :: IO ()
-- main = BugParser1.Main.main

-- main :: IO ()
-- main = BugParser2.Main.main
