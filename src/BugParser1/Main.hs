module BugParser1.Main where

import BugParser1.Parser (maybeToken1)
import BugParser1.Generator (TokenTag (Token1))
import ParserLibrary.RunTimeEnvironment
  (
    Token (Token),
    ParseTree (Sequence, ParsedToken),
  )
import ParserLibrary.RunTimeEnvironment qualified as RTE

import Text.Pretty.Simple (pPrint)
import Data.Text qualified
import Relude

input :: [Token TokenTag]
input = []

result ::
  Either (RTE.ParseError TokenTag) (ParseTree TokenTag)
result = RTE.runParser maybeToken1 input

main :: IO ()
main = pPrint result

-- $> BugParser1.Main.result
