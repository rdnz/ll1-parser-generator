module BugParser1.Parser where
import BugParser1.Generator (TokenTag (..))
import ParserLibrary.RunTimeEnvironment hiding (Parser)
import ParserLibrary.RunTimeEnvironment qualified
import Relude hiding (sequence)
type Parser = ParserLibrary.RunTimeEnvironment.Parser TokenTag (ParseTree TokenTag)
maybeToken1 :: Parser
maybeToken1 =
 do {
 tokenNextMaybe <- peek;
 case tokenNextMaybe of {
 Just tokenNext
 | tokenNext `elem` [] ->  sequence 0 []
 | tokenNext `elem` [Token1] ->  sequence 1 [ token Token1]
 ;
 _ ->  sequence 0 [] }}
