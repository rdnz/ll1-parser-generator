module BugParser1.Generator where

import ParserLibrary.Generator
  (
    Alternative (Alternative),
    Sequence (Sequence),
    Symbol (Terminal),
    writeParser
  )

import Relude hiding (Alternative)

{-
Backus–Naur form

maybeToken1 ::= \epsilon | Token1

-}

data TokenTag =
  Token1
  deriving (Eq, Show)

maybeToken1 :: Alternative
maybeToken1 =
  Alternative
    (
      Sequence [] :|
      Sequence [Terminal "Token1"] :
      []
    )

main :: IO ()
main =
  writeParser
    "BugParser1.Parser"
    "TokenTag"
    "BugParser1.Generator"
    (fromList [("maybeToken1", maybeToken1)])
