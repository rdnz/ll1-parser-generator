module BugParser2.Generator where

import ParserLibrary.Generator
  (
    Alternative (Alternative),
    Sequence (Sequence),
    Symbol (Terminal, Nonterminal),
    writeParser
  )

import Relude hiding (Alternative)

{-
Backus–Naur form

expression ::= maybeToken1 Token2

maybeToken1 ::= \epsilon | Token1

-}

data TokenTag =
  Token1 | Token2
  deriving (Eq, Show)

maybeToken1 :: Alternative
maybeToken1 =
  Alternative
    (
      Sequence [] :|
      Sequence [Terminal "Token1"] :
      []
    )

expression :: Alternative
expression =
  Alternative
    (
      Sequence
        [Nonterminal "maybeToken1" maybeToken1, Terminal "Token2"] :|
      []
    )

main :: IO ()
main =
  writeParser
    "BugParser2.Parser"
    "TokenTag"
    "BugParser2.Generator"
    (fromList [("maybeToken1", maybeToken1), ("expression", expression)])
