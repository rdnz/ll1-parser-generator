module BugParser2.Parser where
import BugParser2.Generator (TokenTag (..))
import ParserLibrary.RunTimeEnvironment hiding (Parser)
import ParserLibrary.RunTimeEnvironment qualified
import Relude hiding (sequence)
type Parser = ParserLibrary.RunTimeEnvironment.Parser TokenTag (ParseTree TokenTag)
expression :: Parser
expression =
 do {
 tokenNextMaybe <- peek;
 case tokenNextMaybe of {
 Just tokenNext
 | tokenNext `elem` [Token2, Token1] ->  sequence 0 [maybeToken1,  token Token2]
 ;
 _ ->  throwUnexpected
 }}
maybeToken1 :: Parser
maybeToken1 =
 do {
 tokenNextMaybe <- peek;
 case tokenNextMaybe of {
 Just tokenNext
 | tokenNext `elem` [] ->  sequence 0 []
 | tokenNext `elem` [Token1] ->  sequence 1 [ token Token1]
 ;
 _ ->  sequence 0 [] }}
