module BugParser2.Main where

import BugParser2.Parser (expression)
import BugParser2.Generator (TokenTag (Token1, Token2))
import ParserLibrary.RunTimeEnvironment
  (
    Token (Token),
    ParseTree (Sequence, ParsedToken),
  )
import ParserLibrary.RunTimeEnvironment qualified as RTE

import Text.Pretty.Simple (pPrint)
import Data.Text qualified
import Relude

input :: [Token TokenTag]
input = [Token Token2 "token2"]

result ::
  Either (RTE.ParseError TokenTag) (ParseTree TokenTag)
result = RTE.runParser expression input

main :: IO ()
main = pPrint result

-- $> BugParser2.Main.result
