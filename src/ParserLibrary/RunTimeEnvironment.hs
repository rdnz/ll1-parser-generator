module ParserLibrary.RunTimeEnvironment where

import Relude hiding (sequence, MonadState (..))
import Relude qualified

-- | The first field specifies the token (kind). The second field
-- contains its payload, that is the text represented by the
-- token. For example, @Token Identifier "myFunction"@. The Parser
-- operates on a stream of values this type.
data Token tokenTag =
  Token tokenTag Text
  deriving (Show)

newtype Parser tokenTag a =
  Parser
    (
      [Token tokenTag] ->
      (Either (ParseError tokenTag) a, [Token tokenTag])
    )

data ParseError tokenTag =
  Unexpected (NonEmpty (Token tokenTag)) (Maybe (Expected tokenTag)) |
  UnexpectedEndOfInput (Maybe tokenTag)
  deriving (Show)

data Expected tokenTag =
  ExpectedToken tokenTag |
  EndOfInput
  deriving (Show)

data ParseTree tokenTag =
  ParsedToken (Token tokenTag) |
  Sequence Natural [ParseTree tokenTag]
  deriving (Show)

instance Functor (Parser tokenTag) where
  fmap f (Parser parser) =
    Parser
      (\tokensOld ->
        let (result, tokensNew) = parser tokensOld
        in (f <$> result, tokensNew)
      )

instance Applicative (Parser tokenTag) where
  pure result = Parser (\tokens -> (Right result, tokens))
  (Parser parser1) <*> (Parser parser2) =
    Parser
      (\tokensOld1 ->
        let
          (result1, tokensOld2) = parser1 tokensOld1
          (result2, tokensNew) = parser2 tokensOld2
        in case result1 of
          Left parseError -> (Left parseError, tokensOld2)
          Right f -> (f <$> result2, tokensNew)
      )

instance Monad (Parser tokenTag) where
  return = pure
  (Parser parser1) >>= f =
    Parser
      (\tokensOld1 ->
         let
           (result1, tokensOld2) = parser1 tokensOld1
         in case result1 of
           Left parseError -> (Left parseError, tokensOld2)
           Right parseResult ->
             let (Parser parser2) = f parseResult
             in parser2 tokensOld2
      )

get :: Parser tokenTag [Token tokenTag]
get = Parser (\tokens -> (Right tokens, tokens))

put :: [Token tokenTag] -> Parser tokenTag ()
put tokens = Parser (\_ -> (Right (), tokens))

throwError :: ParseError tokenTag -> Parser tokenTag void
throwError parseError = Parser (\tokens -> (Left parseError, tokens))

-- newtype Parser tokenTag a =
--   Parser (ExceptT (ParseError tokenTag) (State [Token tokenTag]) a)
--   deriving (
--     Functor, Applicative, Monad,
--     MonadState [Token tokenTag], E.MonadError (ParseError tokenTag)
--   )

single ::
  (Eq tokenTag) => tokenTag -> Parser tokenTag (Token tokenTag)
single tokenTag =
  do
    tokens <- get
    case tokens of
      [] ->
        throwError $ UnexpectedEndOfInput (Just tokenTag)
      tokenNext@(Token tokenNextTag _) : tokensRest ->
        if tokenNextTag == tokenTag
        then put tokensRest *> pure tokenNext
        else
          throwError $
            Unexpected
              (tokenNext :| tokensRest)
              (Just $ ExpectedToken tokenTag)

token ::
  (Eq tokenTag) =>
  tokenTag -> Parser tokenTag (ParseTree tokenTag)
token = fmap ParsedToken . single

sequence ::
  Natural ->
  [Parser tokenTag (ParseTree tokenTag)] ->
  Parser tokenTag (ParseTree tokenTag)
sequence branch parsers =
  Sequence branch <$> Relude.sequence parsers

peek :: Parser tokenTag (Maybe tokenTag)
peek =
  do
    tokens <- get
    case tokens of
      [] -> pure Nothing
      (Token tokenTag _) : _ -> pure $ Just $ tokenTag

throwUnexpected :: Parser tokenTag a
throwUnexpected =
  do
    tokens <- get
    case nonEmpty tokens of
      Nothing -> throwError (UnexpectedEndOfInput Nothing)
      Just tokensNonEmpty ->
        throwError (Unexpected tokensNonEmpty Nothing)

runParser ::
  Parser tokenTag a ->
  [Token tokenTag] ->
  Either (ParseError tokenTag) a
runParser (Parser parser) tokens =
  case parser tokens of
    (parseError@(Left _), _) -> parseError
    (Right result, []) -> Right result
    (_, tokenNext : tokensRest) ->
      Left $ Unexpected (tokenNext :| tokensRest) (Just EndOfInput)
