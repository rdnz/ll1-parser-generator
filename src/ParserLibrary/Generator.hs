module ParserLibrary.Generator where

import Data.Text.Lazy.Builder qualified as B
import Data.Text qualified as T
import Data.Text.Lazy.Builder.Int (decimal)
import Data.Text.Lazy.Encoding qualified
import Data.ByteString.Lazy qualified
import Data.HashMap.Strict qualified as M
import Data.HashSet qualified as S
import Data.List (span)
import Data.List.NonEmpty qualified as NE
import Relude.Extra.Foldable1
import Relude hiding (Alternative)

newtype Alternative =
  Alternative (NonEmpty Sequence)

newtype Sequence =
  Sequence [Symbol]

data Symbol =
  Terminal Text |
  Nonterminal Text Alternative

writeParser ::
  Text -> Text -> Text -> HashMap Text Alternative -> IO ()
writeParser outputModule tokenTagType tokenTagModule rules =
  Data.ByteString.Lazy.writeFile
    (T.unpack $ "src/" <> T.replace "." "/" outputModule <> ".hs") $
  Data.Text.Lazy.Encoding.encodeUtf8 $
  B.toLazyText $
  renderParser outputModule tokenTagType tokenTagModule rules

renderParser ::
  Text -> Text -> Text -> HashMap Text Alternative -> B.Builder
renderParser outputModule tokenTagType tokenTagModule rules =
  "module " <> B.fromText outputModule <> " where\n\
  \import " <> B.fromText tokenTagModule <>
  " (" <> B.fromText tokenTagType <> " (..))\n\
  \import ParserLibrary.RunTimeEnvironment hiding (Parser)\n\
  \import ParserLibrary.RunTimeEnvironment qualified\n\
  \import Relude hiding (sequence)\n\
  \type Parser = ParserLibrary.RunTimeEnvironment.Parser " <>
  B.fromText tokenTagType <>
  " (ParseTree " <> B.fromText tokenTagType <> ")\n" <>
  M.foldr' (<>) mempty (M.mapWithKey renderRule rules)

renderRule :: Text -> Alternative -> B.Builder
renderRule nonterminal alternative =
  B.fromText nonterminal <> " :: Parser\n" <>
  B.fromText nonterminal <> " =\n" <>
  renderAlternative alternative

renderAlternative :: Alternative -> B.Builder
renderAlternative (Alternative sequences) =
  " do {\n\
  \ tokenNextMaybe <- peek;\n\
  \ case tokenNextMaybe of {\n\
  \ Just tokenNext\n" <>
  foldMap' renderSequenceGuarded (NE.zip (0 :| [1..]) sequences) <>
  " ;\n\
  \ _ -> " <> defaultCase <>
  " }}\n"
  where
    defaultCase :: B.Builder
    defaultCase =
      maybe " throwUnexpected\n" renderSequence $
      listToMaybe $
      NE.filter (S.member Nothing . firstOfSequence . snd) $
      NE.zip (0 :| [1..]) $
      sequences

renderSequenceGuarded :: (Natural, Sequence) -> B.Builder
renderSequenceGuarded parameter@(_index, sequence) =
  " | tokenNext `elem` " <>
  (renderList $ fmap B.fromText $ catMaybes $ toList $ firstOfSequence sequence) <>
  " -> " <>
  renderSequence parameter <>
  "\n"

renderSequence :: (Natural, Sequence) -> B.Builder
renderSequence (index, Sequence symbols) =
  " sequence " <> -- not Data.Traversable.sequence
  decimal index <> " " <>
  renderList (fmap renderSymbol symbols)

renderList :: [B.Builder] -> B.Builder
renderList list =
  "[" <> (fold' $ intersperse ", " $ list) <> "]"

renderSymbol :: Symbol -> B.Builder
renderSymbol (Terminal token) = " token " <> B.fromText token
renderSymbol (Nonterminal nonterminal _) = B.fromText nonterminal

firstOfSequence :: Sequence -> HashSet (Maybe Text)
firstOfSequence (Sequence symbols) =
  sequenceUnion $ fmap firstOfSymbol $ symbols
  where
    sequenceUnion ::
      [HashSet (Maybe Text)] -> HashSet (Maybe Text)
    sequenceUnion sets =
      case span (S.member Nothing) sets of
        (containsNothing, rest) ->
          fold' (S.delete Nothing <$> containsNothing)
          <>
          (fromMaybe (S.singleton Nothing) $ listToMaybe $ rest)

firstOfSymbol :: Symbol -> HashSet (Maybe Text)
firstOfSymbol symbol =
  case symbol of
    Terminal token -> S.singleton (Just token)
    Nonterminal _ alternative -> firstOfAlternative alternative

firstOfAlternative :: Alternative -> HashSet (Maybe Text)
firstOfAlternative (Alternative sequences) =
  fold1 $ fmap firstOfSequence $ sequences

fold' :: (Foldable t, Monoid m) => t m -> m
fold' = foldMap' id
