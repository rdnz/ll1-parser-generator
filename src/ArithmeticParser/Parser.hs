module ArithmeticParser.Parser where
import ArithmeticParser.Generator (ArithmeticToken (..))
import ParserLibrary.RunTimeEnvironment hiding (Parser)
import ParserLibrary.RunTimeEnvironment qualified
import Relude hiding (sequence)
type Parser = ParserLibrary.RunTimeEnvironment.Parser ArithmeticToken (ParseTree ArithmeticToken)
operator :: Parser
operator =
 do {
 tokenNextMaybe <- peek;
 case tokenNextMaybe of {
 Just tokenNext
 | tokenNext `elem` [Plus] ->  sequence 0 [ token Plus]
 | tokenNext `elem` [Times] ->  sequence 1 [ token Times]
 ;
 _ ->  throwUnexpected
 }}
expression :: Parser
expression =
 do {
 tokenNextMaybe <- peek;
 case tokenNextMaybe of {
 Just tokenNext
 | tokenNext `elem` [Number] ->  sequence 0 [ token Number]
 | tokenNext `elem` [RoundBracketOpen] ->  sequence 1 [ token RoundBracketOpen, expression, operator, expression,  token RoundBracketClose]
 ;
 _ ->  throwUnexpected
 }}
