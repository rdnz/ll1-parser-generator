module ArithmeticParser.Main where

import ArithmeticParser.Parser (expression)
import ArithmeticParser.Generator
  (ArithmeticToken
    (
      RoundBracketOpen,
      RoundBracketClose,
      Number,
      Plus,
      Times
    )
  )
import ParserLibrary.RunTimeEnvironment
  (
    Token (Token),
    ParseTree (Sequence, ParsedToken),
  )
import ParserLibrary.RunTimeEnvironment qualified as RTE

import Relude.Unsafe qualified as Unsafe
import Text.Pretty.Simple (pPrint)
import Data.Text qualified
import Relude

data Expression =
  Literal Natural |
  Application Expression Operator Expression
  deriving (Show)

data Operator =
  Add | Multiply
  deriving (Show)

expressionFromParseTree :: ParseTree ArithmeticToken -> Expression
expressionFromParseTree parseTree =
  -- Notice how many and detailed assumptions we have to make about
  -- the rather unstructured ParseTree type. This results in a partial
  -- function. The assumptions are only justified by the grammar in
  -- `Generator.hs`. The type checker cannot help.
  case parseTree of
    -- ParsedToken _ ->
    Sequence 0 [ParsedToken (Token Number number)] ->
      Literal $ Unsafe.read $ Data.Text.unpack $ number
    Sequence
      1
      [
        ParsedToken (Token RoundBracketOpen "("),
        expression1,
        operator,
        expression2,
        ParsedToken (Token RoundBracketClose ")")
      ] ->
        Application
          (expressionFromParseTree expression1)
          (operatorFromParseTree operator)
          (expressionFromParseTree expression2)

operatorFromParseTree :: ParseTree ArithmeticToken -> Operator
operatorFromParseTree parseTree =
  case parseTree of
    Sequence 0 [ParsedToken (Token Plus "+")] -> Add
    Sequence 1 [ParsedToken (Token Times "*")] -> Multiply

input :: [Token ArithmeticToken]
input =
  [
    Token RoundBracketOpen "(",
    Token Number "11",
    Token Times "*",
    Token RoundBracketOpen "(",
    Token Number "22",
    Token Plus "+",
    Token Number "33",
    Token RoundBracketClose ")",
    Token RoundBracketClose ")"
  ]

parseTree ::
  Either (RTE.ParseError ArithmeticToken) (ParseTree ArithmeticToken)
parseTree = RTE.runParser expression input

result :: Either (RTE.ParseError ArithmeticToken) Expression
result = expressionFromParseTree <$> parseTree

main :: IO ()
main =
  pPrint parseTree *>
  putTextLn "" *>
  pPrint result

-- $> ArithmeticParser.Main.result
