module ArithmeticParser.Generator where

import ParserLibrary.Generator
  (
    Alternative (Alternative),
    Sequence (Sequence),
    Symbol (Terminal, Nonterminal),
    writeParser
  )

import Relude hiding (Alternative)

{-
Backus–Naur form

operator ::= Plus | Times

expression ::=
  Number |
  RoundBracketOpen expression operator expression RoundBracketClose

-}

data ArithmeticToken =
  RoundBracketOpen |
  RoundBracketClose |
  Number |
  Plus |
  Times
  deriving (Eq, Show)

expression :: Alternative
expression =
  Alternative
    (
      Sequence [Terminal "Number"] :|
      Sequence
        [
          Terminal "RoundBracketOpen",
          Nonterminal "expression" expression,
          Nonterminal "operator" operator,
          Nonterminal "expression" expression,
          Terminal "RoundBracketClose"
        ] :
      []
    )

operator :: Alternative
operator =
  Alternative
    (
      Sequence [Terminal "Plus"] :|
      Sequence [Terminal "Times"] :
      []
    )

main :: IO ()
main =
  writeParser
    "ArithmeticParser.Parser"
    "ArithmeticToken"
    "ArithmeticParser.Generator"
    (fromList [("expression", expression), ("operator", operator)])
