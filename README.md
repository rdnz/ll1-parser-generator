# ll1-parser-generator

Let `prefix` be any of `ArithmeticParser`, `BugParser1`, and
`BugParser2`. Then `src/prefix/Generator.hs` contains a grammar and
its `main` generates a parser in
`src/prefix/Parser.hs`. `src/prefix/Main.hs` contains a sample input,
imports the parser from `src/prefix/Parser.hs` and its `main` applies
the parser to the sample input.
